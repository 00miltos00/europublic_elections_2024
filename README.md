# European Union Election 2024

## Statistic analysis from Greece, Agii Theodoroi, voting center 117

This is a python script that creates some statistics figures with Python and Matplotlib.
The goal is to analyse the voters age distribution and the age distribution of abstention.

The data is extracted only from the official voters catalog for the specific voting center.

This is made for educational purposes only.

Data contains:
- [ ] Voters year of birthday
- [ ] If the person vote or not

Created on 21/06/2024 with:
- [ ] [Arch Linux](https://archlinux.org/)
- [ ] [PyCharm 2024.1.3](https://www.jetbrains.com/pycharm/) (Community Edition) 
- [ ] [Python 3.12.3-1](https://www.python.org/)
- [ ] [Matplolib 3.8.3-3](https://matplotlib.org/)

This project is licenced under CC BY-SA